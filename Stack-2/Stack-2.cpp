﻿#include <iostream>
#include <stack>  

using namespace std;

int main() {
    int Size;
    setlocale(LC_ALL, "rus");
    stack <float> MyStack;  

    cout << "Введите размер стека: "; cin >> Size;
   
    int i = 0;

    cout << "Введите "<< Size<<" любых чисел: " << endl; 
    while (i != Size) {
        float a;
        cout << (i+1)<<" - "; cin >> a;
        MyStack.push(a);  // Добавляем числа в стек
        i++;
    }

    if (MyStack.empty()) cout << "Стек не пуст"; 

    cout << "Извлекаем элементы стека:" << endl;
    i = 0;
    while (i != Size) {
       cout << i <<": "<< MyStack.top() << endl;
       MyStack.pop();
       i++;
    }
   
   system("pause");
    return 0;
}